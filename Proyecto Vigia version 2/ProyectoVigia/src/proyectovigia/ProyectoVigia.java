package proyectovigia;

import java.util.Scanner;

class ProyectoVigia {
    public static void main(String[] args) {
        Administrador admi = new Administrador();
        Scanner obj1 = new Scanner(System.in);
        
        //Ingresa usuario
        System.out.println("Ingresar usuario");
        String usuario = obj1.nextLine();
        
        //Ingresa contraseña
        System.out.println("Ingresar contraseña");
        String contraseña = obj1.nextLine();
        admi.ingresarUsuario(usuario);
        admi.ingresarContraseña(contraseña);
        
        //Ingresa Estacion
        System.out.println("Ingresa Estacion");
        String estacion;
        estacion = obj1.nextLine(); 
        admi.ingresarEstacion(estacion);
        
        //Ingresa numero de empleado
        System.out.println("Ingresar numero de empleado");
        int numEmpleado = obj1.nextInt();
        admi.ingresarEmpleado(numEmpleado);
        
        //Ingresar computadora
        System.out.println("Ingresar computadora");
        String computadora = obj1.nextLine();
        admi.ingresarComputadora(computadora);
 
        //Ingresar impresora
        System.out.println("Ingresar impresora");
        String impresora = obj1.nextLine();
        admi.ingresarImpresora(impresora); 
        
        //Ingresar telefono
        System.out.println("Ingresar telefono");
        int telefono = obj1.nextInt();
        admi.ingresarTelefono(telefono);
        
        //Buscar estacion
        System.out.println("Buscar estacion");
        String nombreEstacion = obj1.nextLine();
        admi.BuscarEstacion(nombreEstacion);
        
        //Editar datos
        
        admi.EditarDatosEncontrados();
        
    }
    
}
