/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Locale;
public class conexion {

    Statement sentencia;
    Connection conexion;
    public conexion(){
        sentencia=null;
        conexion=null;
    }
    public boolean crearConexion() throws ClassNotFoundException{
        boolean estado=false;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Driver Encontrado");
        try{
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/vigia", "root", "");
            System.out.println("Conexion Establecida");
            estado=true;
        }catch(SQLException ex){
            System.err.println("ERROR: No se encontro la bd");
        }
    }catch(ClassNotFoundException cex){
        System.err.println("ERROR: No se encontro el driver");
    }     
   return true;
}
    public boolean ejecutarSQL(String sql){
   try {
      Statement sentencia = conexion.createStatement();
      sentencia.executeUpdate(sql);
   } catch (SQLException ex) {
      ex.printStackTrace();
   return false;
   }
 
   return true;
}
    public ResultSet seleccionar(String sql)
{
 ResultSet resultado;
   try {
       sentencia = conexion.createStatement();
      resultado = sentencia.executeQuery(sql);
   } catch (SQLException ex) {
      ex.printStackTrace();
      return null;
   }
 
   return resultado;
}

}
    

